package agh.qa.utils;

public class StringUtils {
    public static boolean IsNumeric(String string){
        return string.matches("-?\\d+(\\.\\d+)?");
    }

    public static byte[] ToByteArray(String text){
        byte bytes[] = new byte[text.length()];

        for (int i = 0; i < 11; i++){
            bytes[i] = Byte.parseByte(text.substring(i, i+1));
        }

        return bytes;
    }
}